\frame{
  \frametitle{Models of computation}

  \begin{itemize}
  \item {\bf Finite automata and regular expressions.}
    \begin{itemize}
    \item Devices with small, limited memory.
    \end{itemize}
  \item {\bf Push-down automata and context-free languages}
    \begin{itemize}
    \item Devices with unlimited memory, but have restricted access.
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Turing Machines}

  \pause
  
  \begin{itemize}
  \item Proposed by Alan Turing in 1936.
    \pause
  \item A finite automaton with an \structure{unlimited} memory with
    \structure{unrestricted} access.  
    \pause
  \item Can perform any tasks that a computer can.  (we'll see)
    \pause
  \item However, there are problems that TM can't solve.  These
    problems are beyond the limit of computation.
  \end{itemize}
}

\frame{
  \frametitle{Components}

  \begin{itemize}
  \item An infinite \alert{\bf tape}.
  \item A tape head that can 
    \begin{itemize}
    \item \alert{\bf read and write} to the tape, and
    \item \alert{\bf move} around the tape.
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Schematic}

  \begin{center}
    \includegraphics[scale=0.4]{figures/tm.pdf}
  \end{center}
}

\frame{
  \frametitle{How Turing machines work}

  \begin{itemize}
  \item The tape initialy contains an input string.
    \pause
  \item The rest of the tape is blank (denoted by $\sqcup$).
    \pause
  \item The machine reads a symbol from of the tape where its head is
    at.
    \pause
  \item It can write a symbol back and move \alert{left} or
    \alert{right}.
    \pause
  \item At the end of the computation, the machine outputs
    \structure{accept} or \structure{reject}, by entering accept state
    of reject state.  (After changing, it halts.)
    \pause
  \item It can go on forever (not entering any accept or reject
    states).
  \end{itemize}
}

\frame{
  \frametitle{Example: $M_1$}

  \begin{itemize}
  \item We'll design a TM that recognizes 
    \[ 
      B=\{ w{\mathtt \#}w \;|\; w\in\{0,1\}^*\}.  
      \]
  \end{itemize}
}

\frame{
  \frametitle{Example: $M_1$ --- strategy}

  \begin{itemize}
  \item $M_1$ works by comparing two copies of $w$.
  \item $M_1$ compares two symbols on the corresponding positions.
  \item It write marks on the tape to keep track of the position.
  \end{itemize}

}

\frame{
  \frametitle{Example: $M_1$ --- snapshots}

  \noindent
  {\tt
    \al{0} 1 1 0 0 0 \# 0 1 1 0 0 0 $\sqcup$\\ \pause
    x \al{1} 1 0 0 0 \# 0 1 1 0 0 0 $\sqcup$\\ \pause
    x 1 1 0 0 0 \# \al{0} 1 1 0 0 0 $\sqcup$\\ \pause
    x 1 1 0 0 0 \# \al{x} 1 1 0 0 0 $\sqcup$\\ \pause
    \al{x} 1 1 0 0 0 \# x 1 1 0 0 0 $\sqcup$\\ \pause
    x \al{1} 1 0 0 0 \# x 1 1 0 0 0 $\sqcup$\\ \pause
    x x \al{1} 0 0 0 \# x 1 1 0 0 0 $\sqcup$\\ \pause
    x x 1 0 0 0 \# x \al{x} 1 0 0 0 $\sqcup$\\ \pause
    x x x x x x \# x x x x x x \al{$\sqcup$}
  } accept!
}

\frame{
  \frametitle{Example: $M_1$ --- algorithm}

  $M_1 = $ ``On input string $w$:
  \begin{enumerate}
  \item Zig-zag across the tape to corresponding positions on either
    side of the {\tt \#} symbol to check if they contains the same
    symbol.  
    \pause
    If they do not or there is no {\tt \#}, \al{reject}.
    \pause
    Mark these symbols to keep track of the current position.
    \pause
  \item After all symbols on the left of {\tt\#} have been marked,
    check if there're other unmarked symbols on the right of {\tt\#},
    if there's any, \al{reject}; otherwise \al{accept}.''
  \end{enumerate}

}

\frame{
  \frametitle{Formal definition of a Turing Machine}

  \begin{itemize} 
  \item Again, the important part is the definition of
    the transition function.
    \pause
  \item The machine look at the tape \al{symbol} and consider its
    \al{current state}, then makes a move by \al{writing some symbol}
    on the tape and \al{moving its head left or right}.  \pause
  \item Thus,
    \begin{itemize}
    \item {\bf Input:} current state and the symbol on the tape \pause
    \item {\bf Output:} next state, a symbol to be written to the
      tape, and the new state. \pause
    \end{itemize}
    \pause
  \item So, $\delta$ is in the form: $Q\times\Gamma\to
    Q\times\Gamma\times\{\LEFT,\RIGHT\}$ \pause
  \item E.g., if $\delta(q,a)=(r,b,\LEFT)$, then if the machine is in
    state $q$ and reads $a$, it will change its state to $r$, write
    $b$ to the tape and move to the left.
  \end{itemize}

}

\frame{
  \frametitle{Definition}

  \begin{block}{Definition (Turing Machine)}
    A \al{\bf Turing machine} is a 7-tuple,
    $(Q,\Sigma,\Gamma,\delta,q_0,q_{accept},q_{reject})$, where
    $Q,\Sigma,\Gamma$ are finite sets and
    \begin{enumerate}
    \item \al{$Q$} is the set of states,
    \item \al{$\Sigma$} is the input alphabet not containing the
      \al{\bf blank symbol $\sqcup$},
    \item \al{$\Gamma$} is the tape alphabet, where $\sqcup\in\Gamma$
      and $\Sigma\subset\Gamma$,
    \item $\delta: Q\times\Gamma\to
      Q\times\Gamma\times\{\LEFT,\RIGHT\}$ is the transition function,
    \item $q_0\in Q$ is the start state,
    \item $q_{accept}\in Q$ is the accept state, and
    \item $q_{reject}\in Q$ is the reject state, where $q_{accept}\neq
      q_{reject}$.
    \end{enumerate}
  \end{block}
}

\frame{
  \frametitle{Differences between TM and FA}
  \pause
  \begin{itemize}
  \item A Turing machine can read from the tape and write to it.
    \pause
  \item The read-write head can move both to the left and to the
    right.
    \pause
  \item The tape is infinite.
    \pause
  \item The special states for accepting and rejecting take effect
    immediately.
  \end{itemize}
}

\frame{
  \begin{itemize}
  \item A collection of strings that a Turing machine $M$ accepts is
    the language of $M$, denoted by $L(M)$.
    \pause
  \item We say that $M$ \al{recognizes} $L(M)$.
  \end{itemize}
  \pause

  \begin{block}{Definition}
    A language is called \al{\bf Turing-recognizable} if some Turing
    machine recognizes it.
  \end{block}
}

\frame{
  \frametitle{Output of a TM}

  \begin{itemize}
  \item The output of a TM can be \al{accept}, \al{reject}, or
    \al{loop}.
    \pause
  \item A Turing machine may not accept or reject a string.
    \pause
  \item We are interested particularly in TM thats \al{halts} (does
    not loop). \pause We call them \al{\bf decider}. \pause
  \item A decider that recognizes some language also is said to
    \al{\bf decides} that langauge.
  \end{itemize}
}

\frame{
  \begin{block}{Definition}
    A language is called \al{\bf Turing-decidable} or \al{\bf
      decidable} if some Turing machine decides it.
  \end{block}
}

\frame{
  \frametitle{Example: $M_2$}
  
  Design $M_2$ that decides the language $A=\{\zero^{2^n}\;|\; n\geq
  0\}$.

}

\frame{
  \frametitle{$M_2$'s algorithm}

  On input string $w$:
  \begin{enumerate}
  \item Sweep left to right across the tape, crossing off
    \structure{every other $\zero$}.  \pause
  \item If in state 1 the tape contained a single $\zero$,
    \al{accept}.  \pause
  \item If in state 1 the tape contained more than a single $\zero$
    and the number of $\zero$ was odd, \al{reject}. \pause
  \item Return the head to the left-hand end of the tape.
  \item Go to state 1.
  \end{enumerate}
}

\frame{
  \frametitle{Configuration}

  \begin{itemize}
  \item At any point of the computation, the TM can be in some state,
    and at some position on the tape.
    \pause
  \item A \al{\bf configuration} of the Turing machine, ``the current
    computing status'' can be defined with the current state, the
    current position of the tape head, and the content of the tape.
    \pause
  \item We usually write configurate as: $u$ $q$ $v$, where $q$ is the
    state, $uv$ is the current content of the tape, and the TM is at
    the first symbol of $v$.
  \end{itemize}
}

\frame{
  \frametitle{Example execution of $M_2$}
}
