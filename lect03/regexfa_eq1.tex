\frame{
  \frametitle{Equivalence}

  \begin{theorem}
    A language is regular iff some regular expression describes it.
  \end{theorem}

  \pause

  There are two directions to prove the theorem:
  \begin{itemize}
  \item If a language is described by a regular expression, then it is
    regular.  
  \item If a language is regular, then it can be described by a
    regular expression.
  \end{itemize}

  \pause

  Today we'll prove only the first direction.
}

\frame{
  \frametitle{Practice}
  
  \begin{itemize}
  \item Let alphabet $\Sigma=\{\zero,\one\}$.
    \pause
  \item Find an FA $M_1$ that recognizes $\zero\one^+$
    \pause
  \item Find an FA $M_2$ that recognizes $(\one\zero)^*$
    \pause
  \item Find an FA $M_3$ that recognizes $(\zero\one^+)\cup(\one\zero)^*$
    \pause
  \item Find an FA $M_4$ that recognizes
    $\one^+\circ((\zero\one^+)\cup(\one\zero)^*)$ 
  \end{itemize}       
}

\frame{
  \frametitle{A regular expression describes a regular language}

  \begin{lemma}
    If a language is described by a regular expression, then it is
    regular.
    \label{lemma:regex-is-regular}
  \end{lemma}
  
  \pause

  To prove this, we'll look at how we a regular expression is
  constructed.
}

\frame{
  \frametitle{Rule 1}

  $R$ is a \alert{\bf regular expression} if $R$ is $a$ for some
  $a\in\Sigma$.

  \vspace{0.2in}
  \pause
  What is a DFA that recognizes $R$?
}

\frame{
  \frametitle{Rule 2}

  $R$ is a \alert{\bf regular expression} if $R$ is $\varepsilon$.

  \vspace{0.2in}
  \pause
  What is a DFA that recognizes $R$?
}

\frame{
  \frametitle{Rule 3}

  $R$ is a \alert{\bf regular expression} if $R$ is $\emptyset$.

  \vspace{0.2in}
  \pause
  What is a DFA that recognizes $R$?
}


\frame{
  \frametitle{Rule 4}

  $R$ is a \alert{\bf regular expression} if $R$ is $(R_1\cup R_2)$
  where $R_1$ and $R_2$ are regular expressions.

  \vspace{0.2in}
  \pause 
  Given an NFA $N_1$ and $N_2$ that recognize $R_1$ and $R_2$, what is
  a NFA that recognizes $R$?

}

\frame{
  \frametitle{Rule 5}

  $R$ is a \alert{\bf regular expression} if $R$ is $(R_1\circ R_2)$
  where $R_1$ and $R_2$ are regular expressions.

  \vspace{0.2in}
  \pause 
  Given an NFA $N_1$ and $N_2$ that recognize $R_1$ and $R_2$, what is
  a NFA that recognizes $R$?

}

\frame{
  \frametitle{Rule 6}

  $R$ is a \alert{\bf regular expression} if $R$ is $(R_1^{*})$ where
  $R_1$ is a regular expression.  

  \vspace{0.2in}
  \pause 
  Given an NFA $N_1$ that recognizes $R_1$, what is a NFA that
  recognizes $R$?

}

\frame{
  \frametitle{Proof by Structural Induction}
  
  \begin{itemize}
    \item In proving Lemma~\ref{lemma:regex-is-regular}, we show how
      to construct an NFA of a regular expression given NFAs of its
      subexpressions.
      \pause
    \item The proof is again \pause an inductive proof.
      \pause
    \item Sometimes, this kind of inductive proofs is called structural
      induction.
      \pause
      \begin{itemize}
      \item Inductive Hypothesis (when considering a regular
        expression $R$): Assume that for all smaller regular
        expressions $R'$, the language described by $R'$ can be
        recognized by some NFA $N'$.
      \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Practice}
  
  \begin{enumerate}
  \item Find an NFA recognizing $(\zero\one\cup\zero)^*$.
    \pause

    Try to build an NFA using the construction discussed in class.
    \pause
  \item Find an NFA recognizing $(\zero\cup\one)^*\zero\one\zero$.
  \end{enumerate}
}

