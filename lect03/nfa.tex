\frame{
  \frametitle{A machine that always guesses correctly}

  \begin{itemize}
  \item Suppose that our machine can guess where $x$ ends.
  \item It can 
    \begin{itemize}
    \item simulate $M_1$ on the input string up to the end of
      $x$, 
    \item jump to the start state in $M_2$ right after $x$ ends, and
    \item accept string $w=xy$ when the machine stops at some accept
      state in $M_2$.  
    \end{itemize}
  \item Can machine guess?
    \begin{itemize}
    \item Maybe?
    \item But guess correctly?
    \item Ummm.. it definitely can, {\bf in theory.}
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Differences}
  
  \begin{center}
    \includegraphics[scale=0.8]{figures/n1.pdf}
  \end{center}

  \begin{itemize}
  \item Duplicate symbols
  \item Missing symbols
  \item Empty string: $\varepsilon$
  \end{itemize}
}

\frame{
  \frametitle{Deterministic and Nondeterministic Finite Automata}

  \begin{itemize}
  \item Previously, we only consider finite automata whose next states
    are {\bf determined} by their input alphabet and their current
    states.
  \item Computation where each next step is fully determined is called
    \alert{\bf deterministic} computation.
  \item On the other hand, in \alert{\bf nondeterministic}
    computation, many choices may exist.
  \item Therefore, we have deterministic finite automata (\alert{\bf
    DFA}) and nondeterministic finite automata (\alert{\bf NFA}).
  \end{itemize}
}

\frame{
  \frametitle{How does $N_1$ compute?}

  \begin{center}
    \includegraphics[scale=0.8]{figures/n1.pdf}
  \end{center}

  At any point where there are many choices for the next step, the
  machine \alert{\bf splits} itself into many copies and follow all
  possible steps in parallel.

}

\frame{
  \frametitle{Rules for computation of nondeterministic finite automata}
  
  \begin{itemize}
  \item If there are many choices, split.
  \item Copies die if they can't move according to the input.
  \item When to accept a string:
    \begin{itemize}
    \item At the end of the input, if \alert{\bf any} of the copies is
      in an accept state, it \alert{\bf accept} the input.
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{$N_1$ on {\tt 010110}}
  \begin{center}
    \includegraphics[scale=0.35]{figures/computation_n1.pdf}
  \end{center}
}

\frame{
  \frametitle{NFA $N_2$: what are the strings accepted by $N_2$?}

  \begin{center}
    \includegraphics[scale=0.8]{figures/n2.pdf}
  \end{center}  
}

\frame{
  \frametitle{NFA $N_3$: what are the strings accepted by $N_3$?}

  Let $\{0\}$ be the alphabet for $N_3$.

  \begin{center}
    \includegraphics[scale=0.8]{figures/n3.pdf}
  \end{center}  
}

\frame{
  \frametitle{Union (if NFA is allowed)}
  \begin{columns}

    \column{.5\textwidth} {
      \begin{center}
        \includegraphics[scale=0.4]{figures/union_before.pdf}
      \end{center}
    }
  
    \pause

    \column{.5\textwidth} {
      \begin{center}
        \includegraphics[scale=0.4]{figures/union_after.pdf}
      \end{center}
    }
  
  \end{columns}
}


\frame{
  \frametitle{Formal definition of NFAs: $\delta$}

  \begin{itemize}
  \item The transition function $\delta$:
    \begin{itemize}
    \item takes the current state and a symbol in 
      $\Sigma \cup \{\varepsilon\}$, and 
    \item outputs  a subset of states $Q$.
      \pause
    \end{itemize}
  \item Let $\Sigma_{\varepsilon} = \Sigma\cup\{\varepsilon\}$.
  \item Denote by ${\mathcal P}(Q)$ the \alert{\bf power set} of set
    $Q$.
  \item We have that the state transition $\delta$ for an NFA is a
    function from \pause $Q\times\Sigma_{\varepsilon}$ to ${\mathcal P}(Q)$.
  \end{itemize}
}

\frame{
  \frametitle{Definition [NFA]}

  A \alert{\bf nondeterministic finite automaton} is a $5$-tuple
  $(Q,\Sigma,\delta,q_0,F)$, where
  \begin{enumerate}
  \item $Q$ is a finite set of states,
  \item $\Sigma$ is a finite alphabet,
  \item $\delta:Q\times\Sigma_{\varepsilon}\longrightarrow {\mathcal
    P}(Q)$ is the transition function,
  \item $q_0\in Q$ is the start state, and
  \item $F\subseteq Q$ is the set of accept states.
  \end{enumerate}
}

\frame{
  \frametitle{Example: $N_1$}

  \begin{columns}

    \column{0.5\textwidth}{
      \begin{center}
        \includegraphics[scale=0.4]{figures/n1.pdf}
      \end{center}
    }
    
    \column{0.5\textwidth}{
      $N_1$ is $(Q,\Sigma,\delta,q_1,F)$ where
      \begin{enumerate}
      \item $Q=\{q_1,q_2,q_3,q_4\}$,
      \item $\Sigma=\{\zero,\one\}$,
      \item $\delta$ is defined as
        \begin{tabular}{c|ccc}
          & {\tt 0} & {\tt 1} & $\emptyset$ \\
          \hline
          $q_1$ & $\{q_1\}$ & $\{q_1,q_2\}$ & $\emptyset$ \\
          $q_2$ & $\{q_3\}$ & $\emptyset$ & $\{q_3\}$ \\
          $q_3$ & $\emptyset$ & $\{q_4\}$ & $\emptyset$ \\
          $q_4$ & $\{q_4\}$ & $\{q_4\}$ & $\emptyset$
        \end{tabular}
      \item $q_1$ is the start state, and
      \item $F=\{q_4\}$.
      \end{enumerate}
    }
  \end{columns}
}

\frame{
  \frametitle{Formal definition of computation of NFAs}

  Let $N=(Q,\Sigma,\delta,q_0,F)$ be an NFA and let $w$ be a string
  over alphabet $\Sigma$.  

  We say that \alert{\bf $N$ accepts $w$} if we can write
  $w=w_1w_2\cdots w_n$ where each $w_i$ is a member of
  $\Sigma_{\varepsilon}$ and \pause there exists a sequence of states
  $r_0,r_1,\ldots,r_n$ in $Q$ such that \pause
  \begin{enumerate}
  \item $r_0=q_0$,
  \item $r_{i+1}\in\delta(r_i,w_{i+1})$ for $i=0,\ldots, n-1$, and
  \item $r_n\in F$.
  \end{enumerate}
}
