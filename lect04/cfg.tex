\frame{
  \frametitle{Quick recap: Regular languages}

  These sets of languages are equal:
  \begin{itemize}
  \item a set of languages recognized by deterministic finite automata,
  \item a set of languages recognized by nondeterministic finite automata, and
  \item a set of languages described by regular expressions
  \end{itemize}
  \pause
  They are \alert{\bf regular languages}.

  There are languages which are not regular.  Today we will give you
  an example of languages which can be ``described'' by a more powerful mechanism.
}


\frame{
  \frametitle{An example}

  \begin{block}{Grammar $G_1$}
    \begin{eqnarray*}
      A &\rightarrow& \zero A \one\\
      A &\rightarrow& B\\
      B &\rightarrow& \#
    \end{eqnarray*}
  \end{block}

  \pause

  Start with $A$ \pause 
  $\Rightarrow \zero A\one$ (rule 1) \pause
  $\Rightarrow \zero\zero A\one\one$ (rule 1) \pause
  $\Rightarrow \zero\zero B\one\one$ (rule 2) \pause
  $\Rightarrow \zero\zero\#\one\one$ (rule 3). \pause

  This sequence of substitution is called a \alert{\bf derivation}.  

}

\frame{
  \frametitle{A grammar}
  From previous example, you may notice that the grammar has
  \pause
  \begin{itemize}
  \item a set of \alert{substitution rules} (or \alert{production
    rules}), \pause
  \item \alert{variables} (symbols appearing on the left-hand side of
    the arrow), \pause and
  \item \alert{terminals} (other symbols).
  \end{itemize}
  \pause

  To obtain a derivation, we also need a \alert{start
    variable}. \pause (If not specified otherwise, it is the left-hand
  side of the top rule.)

}

\frame{
  \frametitle{From the start variable}

  \begin{itemize}
  \item The grammar $G_1$ \alert{\bf generates} the string
    {\tt 000\#111}.  \pause
  \item How to use the grammar to generate a string: \pause
    \begin{itemize}
    \item Begin with start variable. \pause
    \item Find a variable in the string and a rule that starts with
      that variable.  Replace the variable with the right-hand side of
      the rule. \pause
    \item Repeat.
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{A parse tree}

  \begin{center}
    \includegraphics[scale=0.7]{figures/parsetree-g1.pdf}
  \end{center}
}

\frame{
  \frametitle{Language of the grammar}

  \begin{itemize}
  \item A grammar \alert{\bf describes} a language by generating each
    string of the language.
    \pause
  \item For a grammar \alert{$G$}, let \alert{$L(G)$} denote the
    language of $G$.
    \pause
  \item $L(G_1)=$ \pause $\{\zero^n\#\one^n|n\geq 0\}$
  \end{itemize}
}

\frame{
  \frametitle{A context-free language}

  A language described by some context-free grammar is called a
  \alert{context-free language}.
}

\frame{
  \frametitle{More example}

  \begin{block}{Grammar $G_2$}
    \begin{eqnarray*}
      S &\rightarrow& NP \; VP\\
      NP &\rightarrow& CN | CN \; PP \\
      VP &\rightarrow& CV | CV \; PP \\
      PP &\rightarrow& PREP \; CN\\
      CN &\rightarrow& ART \; N\\
      CV &\rightarrow& V | V \; NP\\
      ART &\rightarrow& \mathtt{a} | \mathtt{the}\\
      N &\rightarrow& \mathtt{boy} | \mathtt{girl} | \mathtt{flower}\\
      V &\rightarrow& \mathtt{touches} | \mathtt{likes} | \mathtt{sees}\\
      PREP &\rightarrow& \mathtt{with}\\
    \end{eqnarray*}
  \end{block}
}

\frame{
  \frametitle{Small English grammar}

  \begin{itemize}
  \item Examples of strings in $L(G_2)$ are:
    \begin{itemize}
    \item {\tt a boy sees}\pause    
    \item {\tt the boy sees a flower}\pause    
    \item {\tt a girl with a flower likes the boy}
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Derivation}

  \begin{itemize}
  \item Show the derivation of string ``{\tt a boy sees}''.
    \pause
  \item Try to generate more strings from $G_2$
    and find their parse trees.
  \end{itemize}
}

\frame{
  \frametitle{Definition [context-free grammar]}
  
  \begin{block}{Definition}
    A \alert{\bf context-free grammar} is a 4-tuple
    \alert{$(V,\Sigma,R,S)$}, where
    \begin{enumerate}
    \item \alert{$V$} is a finite set called the \alert{\bf
      variables},
    \item \alert{$\Sigma$} is a finite set, disjoint from $V$, called
      the \alert{\bf terminals},
    \item \alert{$R$} is a finite set of \alert{\bf rules}, with each
      rule being a variable and a string of variables and terminals,
      and
    \item \alert{$S\in V$} is the \alert{\bf start variable}.
    \end{enumerate}
  \end{block}
  
}

\frame{
  \frametitle{More definitions}
  
  \begin{itemize}
  \item Let $u,v$, and $w$ be strings of variables and terminals, and
    $A\rightarrow w$ be a rule of the grammar.
  \item We say that $uAv$ \alert{\bf yields} $uwv$, \pause denoted by
    $uAv\Rightarrow uwv$. \pause
  \item We say that $u$ \alert{\bf derives} $v$, \pause written as
    $u\derives v$,
    \pause
    \begin{itemize}
    \item if $u=v$, or \pause
    \item if a sequence $u_1,u_2,\ldots, u_k$ exists for $k\geq 0$ and
      \[
        u\Rightarrow u_1\Rightarrow u_2\Rightarrow \cdots\Rightarrow
        u_k\Rightarrow v.
        \]
    \end{itemize}
  \end{itemize}
}

\frame{
  \frametitle{Example: $G_3$}

  $G_3 =(\{S\},\{a,b\},R,S)$, where $R$ is
      \[
        S\rightarrow aSb|SS|\varepsilon.
        \]
}

\frame{
  \frametitle{Practice}
  Find a CFG that describes the following language

  \[
    \{\mathtt{a}^i\mathtt{b}^j\mathtt{c}^k\;|\;\mbox{$i,j,k\geq 0$ and $i=j$ or $j=k$}\}
    \]
}

\frame{
  \frametitle{Example: $G'_4$}

  $G'_4 = (V,\Sigma,R,EXPR)$, where
  \begin{itemize}
  \item $V=\{EXPR\}$, \pause
  \item $\Sigma=\{a,+,\times,(,)\}$, \pause
  \item the rules are

    \begin{eqnarray*}
      EXPR &\rightarrow& EXPR + EXPR \; | \; EXPR\times EXPR \; | \;
      (EXPR) \;|\; a \\
    \end{eqnarray*}
  \end{itemize}

  Generate some string from $G'_4$.
}

\frame{
  \frametitle{Ambiguity}

  Find a parse tree for $a + a\times a$ in grammar $G'_4$.

}

\frame{
  \frametitle{Example: $G_4$}

  $G_4 = (V,\Sigma,R,EXPR)$, where
  \begin{itemize}
  \item $V=\{EXPR,TERM,FACTOR\}$, \pause
  \item $\Sigma=\{a,+,\times,(,)\}$, \pause
  \item the rules are
    
    \begin{eqnarray*}
      EXPR &\rightarrow& EXPR + TERM | TERM \\
      TERM &\rightarrow& TERM \times FACTOR | FACTOR \\
      FACTOR &\rightarrow& (EXPR) | a
    \end{eqnarray*}
  \end{itemize}
}
